<?php
require_once('common.php');

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict// EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		
		<div id="title_block">
			<table>
			<tr>
				<td>
					<img src="images/logo.gif" alt="" width="197" height="52" style="margin:10px 0 0 55px;" />
				</td>
				<td>
					<table>';
					if(isset($_SESSION['validUser'])){	
						if($_SESSION['validUser']== false)
						{
							echo'	
								<tr>
									<td><a href="login.php" style="margin-left:575px">Login</a>&nbsp &nbsp<a href="cart.php">Cart</a> &nbsp &nbsp<a href="register.php">Register</a></td>
								</tr>';
						}		
						if($_SESSION['validUser']== true){
							echo'	
								<tr>
									<td><a href="logout.php" style="margin-left:575px">Logout</a>&nbsp &nbsp<a href="cart.php">Cart</a>
								</tr>';

						}
					}	
				echo'</table>
				</td>
			</tr>
			</table>	
			<div id="navi_block">
				<ul class="nav">
					<li class="nav"><a href="index.php" class="navi_tx" style="color:#000000;">HOME</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="products.php" class="navi_tx">PRODUCTS</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="contact.html" class="navi_tx">CONTACT US</a></li>
				</ul>	
			</div>	
		</div>			 
	</head>
	
	<body>
      
		<div id="main_block">
			<div id="sub_block1">
				<marquee behavior="alternate">
					<image src="images/thumb-1.jpg" alt="display1" style="height:200px;width:200px;">
					<image src="images/thumb-2.jpg" alt="display2" style="height:200px;width:200px;">
					<image src="images/thumb-3.jpg" alt="display3" style="height:200px;width:200px;">
					<image src="images/thumb-4.jpg" alt="display4" style="height:200px;width:200px;">
				    <image src="images/gallery-img5.jpg" alt="display5" style="height:200px;width:200px;">
                    <image src="images/gallery-img4.jpg" alt="display6" style="height:200px;width:200px;">
                    <image src="images/gallery-img3.jpg" alt="display7" style="height:200px;width:200px;">			
					<image src="images/gallery-img2.jpg" alt="display8" style="height:200px;width:200px;">					
					<image src="images/gallery-img1.jpg" alt="display9" style="height:200px;width:200px;">										
				</marquee>
			</div>
			
			<div id="sub_header" style="margin-left:8px;">
				Our Clients
			</div>
			
			<div id="sub_block2" style="margin-left:8px;">
				<table width="100%">
					<tr>
						<td><image src="images/clients.jpg" alt="client_image" style="height:300px;width:300px;"></td>
						<td width="70%">OUR CLIENTS ARE ALWAYS SATISFIED !!!!!!!!!!</td>
					</tr>
				</table>
			</div>
			
			<div id="sub_header" style="margin-left:900px;">
				Our Work
			</div>
			
			<div id="sub_block2" style="margin-left:90px;">
				<table>
					<tr>
						<td><div id="gridImg"><img src="images/page4-img4.jpg" alt="work_img1" /></div></td>
				        <td><div id="gridImg"><img src="images/page4-img5.jpg" alt="work_img1" /></div></td>
				        <td><div id="gridImg"><img src="images/page4-img6.jpg" alt="work_img1" /></div></td>					
					</tr>
					<tr>
						<td><div>MASTER PIECE</div></td>
				        <td><div>AWESOME DESIGN</div></td>
				        <td><div>BEST STRUCTURE</div></td>						
					</tr>
				</table>
			</div>
		<div>
		
	</body>
	
	<footer>
		<div id="footer_block">
        &copy;Furniture Collections Website 2013 | <a href="http://validator.w3.org/check?uri=referer"><img
    src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a> | <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border:0;width:88px;height:31px"
      src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
      alt="Valid CSS!" /></a><br/>
      <a href="index.php">Back to Top</a><br/>
      <a href="privacypolicy.html">Privacy and Policies</a>
    </div>	
	</footer>
</html>';
?>