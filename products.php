<?php
require_once('common.php');

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict// EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		
		<div id="title_block">
			<table>
			<tr>
				<td>
					<img src="images/logo.gif" alt="" width="197" height="52" style="margin:10px 0 0 55px;" />
				</td>
				<td>
					<table>';
						if($_SESSION['validUser']== false)
						{
							echo'	
								<tr>
									<td><a href="login.php" style="margin-left:575px">Login</a>&nbsp &nbsp<a href="cart.php">Cart</a> &nbsp &nbsp<a href="register.php">Register</a></td>
								</tr>';
						}		
						if($_SESSION['validUser']== true){
							echo'	
								<tr>
									<td><a href="logout.php" style="margin-left:575px">Logout</a>&nbsp &nbsp<a href="cart.php">Cart</a>
								</tr>';
						}
						
				echo'</table>
				</td>
			</tr>
			</table>
			
			<div id="navi_block">
				<ul class="nav">
					<li class="nav"><a href="index.php" class="navi_tx">HOME</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="products.php" class="navi_tx" style="color:#000000;">PRODUCTS</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="contact.html" class="navi_tx">CONTACT US</a></li>
				</ul>	
			</div>	
		</div>			 
	</head>
			
	<body>	
		<div id="main_block" style="overflow:scroll;">';
			if(isset($_GET['name'])){
				if($_GET['action']=='add'){
					echo "<div>" . $_GET['name'] . " was added to your cart.</div>";
				}

				if($_GET['action']=='exists'){
					echo "<div>" . $_GET['name'] . " already exists in your cart.</div>";
				}
			}	
			$file_handle= fopen("products1.txt","a+");
			
			$line_count = 1;	
				while (!feof($file_handle) ) {
				
				$line_of_text = fgets($file_handle);
				$product= explode("\t",$line_of_text);
				
				echo'<div id="sub_header">'
						.$product[1].
					'</div>
					
					<div id="sub_block2" style="padding-top:40px;">			
						<table width="100%;">
						<tr>					
							<td width="30%;"><a href="product.php?line_no='.$line_count.'">
									<img src="'.$product[2].'" alt="product" style="margin-left:50px;margin-top:-30px;"/>
								</a>	
							</td>
							<td width="60%;">
								'.$product[3].'
								<p>'.$product[4].'</p>';
								if($_SESSION['validUser']== false)
								{
									echo'<p>'.$product[5].'</p>';
								}
								else{
									echo'<p>'.$product[7].'</p>';
								}
					echo'</td>
						 <td width="10%;">	
							<p><a href="order.php">SHOP</a></p>
						 </td>
						</tr>
						</table>				
					</div>';
			$line_count++;	
			}fclose($file_handle);
	echo'</body>
	
	<footer>
		<div id="footer_block">
        &copy;Furniture Collections Website 2013 | <a href="http://validator.w3.org/check?uri=referer"><img
    src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a> | <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border:0;width:88px;height:31px"
      src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
      alt="Valid CSS!" /></a><br/>
      <a href="index.php">Back to HomePage</a><br/>
      <a href="privacypolicy.html">Privacy and Policies</a>
    </div>	
	</footer>
</html>';
?>