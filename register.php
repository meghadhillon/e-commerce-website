<?php
	require_once('common.php');
	
	if (isset($_POST['submit'])){
		// Get user input
		$username   = isset($_POST['user_name']) ? $_POST['user_name'] : '';
		$password1  = isset($_POST['password1']) ? $_POST['password1'] : '';
		$password2  = isset($_POST['password2']) ? $_POST['password2'] : '';
		$email      = isset($_POST['email']) ? $_POST['email'] : '';
		$contact_no = isset($_POST['contact_no']) ? $_POST['contact_no'] : '';
        
		// Try to register the user
		$error = registerUser($username,$password1,$password2,$email,$contact_no);
	}	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict// EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		
		<div id="title_block">
			<img src="images/logo.gif" alt="" width="197" height="52" style="margin:50px 0 0 55px;" />
			<div id="navi_block">
				<ul class="nav">
					<li class="nav"><a href="index.php" class="navi_tx">HOME</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="products.php" class="navi_tx">PRODUCTS</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="contact.html" class="navi_tx">CONTACT US</a></li>
				</ul>	
			</div>	
		</div> 		
	</head>
	
	<body>
		
		<div id="main_block">
			
			<div id="sub_header">    
				<?php if ((!isset($_POST['submit'])) || ($error != '')) {;?>
					  
					  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="registerform">
						<table width="100%">
						  <tr><td>Username:</td><td> <input class="text" name="user_name" type="text" /></td></tr>
						  <tr><td>Password:</td><td> <input class="text" name="password1" type="password"/></td></tr>
						  <tr><td>Confirm password:</td><td> <input class="text" name="password2" type="password" /></td></tr>
						  <tr><td>Email:</td><td> <input class="text" name="email" type="text" /></td></tr>
						  <tr><td>Contact No:</td><td> <input class="text" name="contact_no" type="text" maxlength="10"/></td></tr>
						  <tr><td colspan="2" align="center"><input class="text" type="submit" name="submit" value="Register" /></td></tr>
						</table>  
					  </form>
				<?php } ?>
			
				<?php if (isset($_POST['submit'])){ ?>
					  
				<?php
					if ($error == '') {
						echo " User: $username was registered successfully!<br/><br/>";
						echo ' <a href="login.php">You can login here</a>';
					} else {
						echo $error;
					}
				?>
						<br/><br/><br/></td></tr></table>
		    </div>
				<?php } ?>
        </div>	
	</body>   
</html>