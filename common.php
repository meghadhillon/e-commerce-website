<?php
session_start();

function registerUser($user,$pass1,$pass2,$email,$cno){
	$errorText = '';
	
	// Check passwords
	if ($pass1 != $pass2) {
		$errorText = "Passwords are not identical!";
	} elseif (strlen($pass1) < 8) {
		$errorText = "Password is to short!";
	}
	
	// Check user existance	
	$pfile = fopen("users.txt","a+");
    rewind($pfile);

    while (!feof($pfile)) {
        $line = fgets($pfile);
        $tmp = explode("\t", $line);
        if ($tmp[0] == $user) {
            $errorText = "The selected user name is taken!";
            break;
        }
    }
	
	//Check the regular expression for email
	/*if($email !== "") { 
        if(!ereg("^[A-Za-z0-9\.|-|_]* 
          [@]{1}[A-Za-z0-9\.|-|_]*[.]{1}[a-z]{2,5}$", $email)) { 
			$errorText = "The email id is not in valid form";
			break;
        }
    }*/
	
	
	//Check the regular expression for contact no
	if($cno !== "") { 
        if(ereg("^((\([0-9]{3}\) ?)|([0-9]{3}-))?[0-9]{3}-[0-9]{4}$",$cno)){ 
			$errorText = "The phone no is not in valid form";
            break;
        }
    }
	
    // If everything is OK -> store user data
    if ($errorText == '') {
		// Secure password string
		$userpass = md5($pass1);

		fwrite($pfile, "\r\n$user\t$userpass\t$email\t$cno");
    }

    fclose($pfile);
	
	return $errorText;
}

function loginUser($user,$pass){
		$errorText = '';
		$validUser = false;
		
		// Check user existance	
		$pfile = fopen("users.txt","r");
		rewind($pfile);

		while (!feof($pfile)) {
			$line = fgets($pfile);
			$tmp = explode("\t", $line);
			if ($tmp[0] == $user) {
				// User exists, check password
				if (trim($tmp[1]) == trim(md5($pass))){
					$validUser= true;
					$_SESSION['username'] = $user;
				}
				break;
			}
		}
		fclose($pfile);

		if ($validUser != true) $errorText = "Invalid username or password!";
		
		if ($validUser == true) 
			$_SESSION['validUser']= true;
		else 
			$_SESSION['validUser'] = false;
		
		return $errorText;	
}

function logoutUser(){
	unset($_SESSION['validUser']);
	unset($_SESSION['userName']);
	$_SESSION['validUser'] = false;
}

function checkUser(){
	if ((!isset($_SESSION['validUser'])) || ($_SESSION['validUser'] != true)){
		header("Location:index.php");
	}
}

?>