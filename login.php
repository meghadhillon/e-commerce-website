<?php
require_once('common.php');

$error = '0';

if (isset($_POST['submit'])){
	// Get user input
	$username = isset($_POST['username']) ? $_POST['username'] : '';
	$password = isset($_POST['password']) ? $_POST['password'] : '';
        
	// Try to login the user
	$error = loginUser($username,$password);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict// EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		
		<div id="title_block">
			<img src="images/logo.gif" alt="" width="197" height="52" style="margin:50px 0 0 55px;" />
			<div id="navi_block">
				<ul class="nav">
					<li class="nav"><a href="index.php" class="navi_tx">HOME</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="products.php" class="navi_tx">PRODUCTS</a></li>
					<span class="navi_tx">|</span>
					<li class="nav"><a href="contact.html" class="navi_tx">CONTACT US</a></li>
				</ul>	
			</div>	
		</div>			 
	</head>
	
	<body>
      
		<div id="main_block">
		
			<div id="sub_header"><body>
    
				<?php if ($error != '') {?>
      
				  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="loginform">
					<table width="100%">
					  <tr><td>Username:</td><td> <input class="text" name="username" type="text"  /></td></tr>
					  <tr><td>Password:</td><td> <input class="text" name="password" type="password" /></td></tr>
					  <tr><td colspan="2" align="center"><input class="text" type="submit" name="submit" value="Login" /></td></tr>
					</table>  
				  </form>
				        
					<?php 
					}   
						if (isset($_POST['submit'])){

					?>

					<?php
						if ($error == '') {
							
							header("Location:index.php");
						}
						else echo $error;

					?>
							<br/><br/><br/></td></tr></table>
			</div>
				<?php            
					}
				?>
		</div>		
	</body>   
</html>