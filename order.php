<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict// EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>
		
	<body>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="orderform">
	<table>
	<tr><td>Products </td><td>Quantity</td></tr>
	<tr><td>Sofa Bed</td><td><input type="text" name="sofabedqty" size="3" maxlength="3"/></td></tr> 
	<tr><td>Outdoor Lounger</td><td><input type="text" name="outdoorqty" size="3" maxlength="3"/></td></tr> 
	<tr><td>Chairs</td><td><input type="text" name="chairsqty" size="3" maxlength="3"/></td></tr> 
	<tr><td>Coffee Table</td><td><input type="text" name="tableqty" size="3" maxlength="3"/></td></tr> 
	<tr><td>Mattresses</td><td><input type="text" name="mattressqty" size="3" maxlength="3"/></td></tr> 
	<tr><td>Delivery Address</td><td><input type="text" name="address" size="30" maxlength="30"/></td></tr>
	<tr><td colspan="2"><input type="submit" value="submit"/></td></tr>  
	</table>
	</form>
	<?php
	$sofabedqty = $_POST['sofabedqty'];
	$outdoorqty = $_POST['outdoorqty'];
	$chairsqty = $_POST['chairsqty'];
	$tableqty = $_POST['tableqty'];
	$mattressqty = $_POST['mattressqty'];
	$address = $_POST['address'];

	$total = 0;
	$total += $sofabedqty;
	$total += $outdoorqty;
	$total += $chairsqty;
	$total += $tableqty;
	$total += $mattressqty;
	$totalamount = 0.00;
	define("price_sofabed", 80.99);
	define("price_outdoor", 60.79);
	define("price_chairs", 70.99);
	define("price_table", 30.89);
	define("price_mattress", 90.00);
	$date = date("H:i, jS F");
	echo "<p> Order Details are as follows:";
	echo "<p> Date <br><br>";
	echo $date;
	echo "<br><br>";
	if ($total <= 0)
	{
		echo "No orders <br>";
	}
	else
	{
		if($sofabedqty>0)
			echo $sofabedqty . " sofabed<br>";
		if($outdoorqty>0)
			echo $outdoorqty . " outdoor<br>";
		if($chairsqty>0)
			echo $chairsqty . " chairs<br>";
		if($tableqty>0)
			echo $tableqty . " table<br>";
		if($mattressqty>0)
			echo $mattressqty . " mattress<br>";
	}
	$totalamount = $sofabedqty*price_sofabed + $outdoorqty*price_outdoor + $chairsqty*price_chairs +$tableqty*price_table +$mattressqty*price_mattress;
	$totalamount = number_format($totalamount,2,"."," ");
	echo "<p> Total is : ".$totalamount."</p>";
	echo "<p> Delivery Address is :".$address."<br>";

	$output = "\t"
	.$sofabedqty." sofabed \t"
	.$outdoorqty." outdoor lounger \t"
	.$chairsqty." chairs \t"
	.$tableqty. "table \t"
	.$mattressqty." mattress \t"
	."\n".$totalamount
	."\n".$address
	."\n";

	$fp = fopen("orders.txt", "a");
	fwrite($fp, $output);
	fclose($fp);

	?>

	</body>
</html>